import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {
  public checkBoxStateOne: boolean = false; 

  constructor(private serviceService: StateService) { }

  ngOnInit(): void {
    this.serviceService.reusableComponentState.subscribe((bool: boolean): void => { this.checkBoxStateOne = bool; })
  }

}
