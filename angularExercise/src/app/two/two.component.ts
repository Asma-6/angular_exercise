import { Component, OnInit } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.scss']
})
export class TwoComponent implements OnInit {
  public checkBoxStateTwo: boolean = false; 

  constructor(private serviceService: StateService) { }

  ngOnInit(): void {
    this.serviceService.reusableComponentState.subscribe((bool: boolean): void => { this.checkBoxStateTwo = bool; })
  }
}
