import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: 'routeOne', 
    loadChildren: () => import('./one/one.module').then(m => m.OneModule) 
  }, 
  { 
    path: 'routeTwo',
    loadChildren: () => import('./two/two.module').then(m => m.TwoModule) 
  }
]; // It implies that when routeOne or routeTwo is visited, load their respective modules lazily.

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
