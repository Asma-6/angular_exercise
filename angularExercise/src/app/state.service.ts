import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  //public reusableComponentState = new Subject<boolean>();  
  public reusableComponentState = new BehaviorSubject(false);


  constructor() { }

}
