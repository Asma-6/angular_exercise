import { Component, OnInit } from '@angular/core';
import { StateService } from '../../state.service';

@Component({
  selector: 'app-reusable',
  templateUrl: './reusable.component.html',
  styleUrls: ['./reusable.component.scss']
})
export class ReusableComponent implements OnInit {
  public checkBoxState: boolean = false; 

  constructor(private serviceService: StateService) { }

  ngOnInit(): void {
    this.serviceService.reusableComponentState.subscribe((bool: boolean): void => { this.checkBoxState = bool; })
  }

  public checkBoxChanged(checked: boolean){
    this.serviceService.reusableComponentState.next(checked);
  }
  
}
