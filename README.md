# Angular_Exercise
It's about a simple exercise to initialize in Angular.

## Problem with Subject type of RxJs
First by using the Subject type we need to initialize the checkbox states of the two modules One and Two, so when we change the route from one to two or the reverse, we get the initialized value of the appropriate module and not the current state of the reusable module.

## Solving Problem
To solve this problem, we simply need the modify the type of RxJs type to BehaviorSubject so we can initialize our checkbox state in the serviceState, and even if we change the route the current state remains the same.

## The difference between Subject and BehaviorSubject
As mentioned above, we resolved the problem using BehaviorSubject, so the Subject and the BehaviorSubject is both a special type of Observable that allows values to be multicasted to many Observers. But the difference between them is that the first does not return the current value on Subscription, however the second can hold one value (the initialized one), and When it is subscribed it emits the value immediately.
